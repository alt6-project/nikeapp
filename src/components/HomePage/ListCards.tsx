import React from 'react'
import ProductCard from './ProductCard'
import { urlFor } from '@/sanity'


interface Props{
    products: any
}

function ListCards({products}:Props) {
return (
    <div className="flex space-x-4 overflow-scroll">
    {
        products && products.length > 0 
        ?
        products.map((product:any) =>
        <ProductCard 
            key={product?._id}
            slug={product?.product.slug.current}
            src={product?.mainImage ? urlFor(product?.product.mainImage).url()! : `/Images/chaussure-dunk-low-retro-pour-VMwkPQ.png`}
            title={product?.product.name} 
            description={product?.product.description} 
            price={product?.product.price}
            color={product?.color.color}
            />
        )
        :
        <span>
        {`No products yet`}
        </span>
    }
</div>
)
}

export default ListCards
