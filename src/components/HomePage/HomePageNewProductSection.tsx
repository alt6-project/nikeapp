import React from 'react'
import ListCards from './ListCards'

interface Props{
    products: any
}

function HomePageNewProductSection({products}:Props) {
    return (
    <section 
    className='w-full mt-4 mb-4 px-16 flex flex-col space-y-6'>
    <div className="title">
        <h2>{'Nouveau cette semaine'}</h2>
    </div>
    <ListCards products={products}/> 
</section>
)
}

export default HomePageNewProductSection
