import Link from 'next/link'
import React from 'react'

function HomePageBar() {
return (
    <div 
    className="flex justify-center items-center w-full px-12 py-4 bg-black/5">
        <div className='text-center text-gray-500'>
            <p className='/'>{`Nouveautés de l'année`}</p>
            <Link 
            href={'/'}>
                <p className="underline text-sm ">{"Acheter"}</p>
            </Link>      
        </div>
    </div>
)
}

export default HomePageBar
