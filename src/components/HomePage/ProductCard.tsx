import React from 'react'
import Image from 'next/image'
import Link from 'next/link';

interface ProductCardProps {
    slug:string;
    src: string;
    title: string;
    description:string;
    price?:number;
    color?:string;
}

function ProductCard({slug,src,title,description,price,color}:ProductCardProps) {
return (
    <Link 
        /* href={'/product/'+} */
        href={{ pathname: '/products/{slug}'}}
        className="lg:flex flex-col flex-shrink-0 space-y-4 w-[500px] ">
        <div className="relative w-full h-[500px]">
            {src && src.length>0?(
            <Image
                src={src}
                alt=""
                fill
                className="object-cover"
                />
            ):(
                <p>No Image</p>
            )}
        </div>
        <div>
            <h3 className="font-medium">{title}</h3>
            <p className="text-[#707072]">{description}</p>
            <p className="font-medium block mt-2">{price?.toFixed(2)} €</p>
            <p className="bg-black text-white text-center w-16">{color}</p>
        </div>
    </Link>
)
}

export default ProductCard
