import React, { useState } from 'react'
import Image from 'next/image'

function JordanTop() {
    const [isHovered, setIsHovered] = useState(false);
return (  
    <>
        <div className="flex  w-full h-[80vh] style={{ fontFamily: 'Poppins, sans-serif' }}">
            <div className="bg-black w-[70%] h-full flex flex-col items-center        justify-center ">
                <div className="flex flex-col justify-start gap-16 ">
                    <div>
                    <h2 className="font-bold text-[36px] ">
                        <span className="text-[#F85454]">JOR</span>
                        <span className="text-white">DAN</span>
                    </h2>
                    </div>
                <div className="flex flex-col">
                    <h2 className="text-white font-bold text-[80px] m-0 p-0">NOUVEAU</h2>
                    <h2 className="text-white font-bold text-[80px] m-0 p-0" >OFF-<span className="text-[#F85454]">WHITE</span></h2>
                </div>
                <div className={`relative w-[75px] h-[75px]  ${isHovered ? 'rotate-12' : ''}`}>
                    <Image
                        src={`/Images/pngegg.png`}
                        alt={``}
                        fill
                        className='object-contain'
                    />
                </div>
                </div>
            </div>
        <div className="bg-white flex-1 h-full"></div>
    </div>
    <div className="absolute inset-0 flex items-end justify-center flex-col m-4">
        <div className= {`relative w-[700px] h-[600px] ${isHovered ? '-rotate-12' : ''}`}>
        <Image
        src={`/Images/image1.png`}
        alt={``}
        fill
        className='object-cover'
        />
        </div>
    <div className="flex gap-16 overflow-x-auto w-[700px]">
    <div className="slideimage">
        <Image
            src={`/Images/image4.png`}
            alt={``}
            fill
            className='object-cover'
        />
    </div>
    <div className="slideimage">
        <Image
            src={`/Images/image3.png`}
            alt={``}
            fill
            className='object-cover'
        />
    </div>
    <div className="slideimage">
        <Image
            src={`/Images/image2.png`}
            alt={``}
            fill
            className='object-cover'
        />
    </div>    
</div>  
    </div> 
<div className="absolute inset-x-0 bottom-[100px] right-[400px] flex items-end justify-center m-4 ">
    <button 
    className="px-8 py-4 bg-[#F85454] text-white font-semibold rounded-3xl hover:opacity-40 text-[24px]"
    onMouseEnter={() => setIsHovered(true)}
    onMouseLeave={() => setIsHovered(false)}>
        VOIR PLUS
    </button>
</div>  
    </>
)
}

export default JordanTop
