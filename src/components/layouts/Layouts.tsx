import { Inter } from 'next/font/google'
import React from 'react'
import AboutNav from './AboutNav'
import Header from './Header'
import Head from 'next/head'
import Footer from './Footer'
import Link from 'next/link'
import Loading from './Loading'
const inter = Inter({ subsets: ['latin'] })

function Layouts({children}:any) {
return (
    <div 
    className={`w-screen min-h-screen flex flex-col flex-1 overflow-x-hidden
    ${inter?.className}`}>
    <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="keywords" content={'keywords'} />
        <meta name="description" content={'description'} />
        <meta charSet="utf-8" />
        <title>{'Nike'}</title>
        <link rel="icon" href="/Logos/nike.png" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"></link>
    </Head>
<Loading/>

<AboutNav/>
<Header/>
    <main
    className='flex flex-col flex-1'
    >
    {children}
    </main>
    

    <Footer/>

    
    </div>
)
}

export default Layouts
