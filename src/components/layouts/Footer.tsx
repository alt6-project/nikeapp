import { PlusCircleIcon } from '@heroicons/react/24/outline';
import { MapIcon, MapPinIcon } from '@heroicons/react/24/solid'
import Link from 'next/link'
import React, { useState } from 'react'

function Footer() {
    const [openMenu1, setOpenMenu1]= useState(false);
    const [openMenu2, setOpenMenu2]=useState(false);
    const [openMenu3, setOpenMenu3]=useState(false);
return (
    <div className="bg-black text-white w-full flex flex-col gap-10 items-align pt-10 px-16">
    <div className="lg:flex justify-between">
        <div className="lg:flex justify-between gap-20 font-light text-[12px]">
            <div className="flex flex-col gap-2 mb-5 ">
                <Link
                href={"/"}><p>{"CARTES CADEAUX"}</p></Link>
                <Link
                href={"/"}><p>{"TROUVER UN MAGASIN"}</p></Link>
                <Link
                href={"/"}><p>{"DEVENIR MEMBRE"}</p></Link>
                <Link
                href={"/"}><p>{"RÉDUCTION POUR ÉTUDIANTS"}</p></Link>
                <Link
                href={"/"}><p>{"COMMENTAIRES"}</p></Link>
                <Link
                href={"/"}><p>{"CODES PROMOS"}</p></Link>
            </div>
            <div className="flex flex-col gap-2 mb-5">
                <div onClick={()=>{
                    setOpenMenu1(!openMenu1)
                }}
                    className="flex flex-row justify-between lg:pointer-events-none">
                    <p className="block">{"AIDE"}</p>
                    <PlusCircleIcon className="lg:hidden w-7"/>
                </div>
                <div  className={`font-extralight text-gray-300 text-[12px] flex flex-col gap-2 lg:block ${
                openMenu1 ? 'block' : 'hidden'
                }`}>
                <Link
                href={"/"}><p>{"Statut de commande"}</p></Link>
                <Link
                href={"/"}><p>{"Expédition et livraison"}</p></Link>
                <Link
                href={"/"}><p>{"Retours"}</p></Link>
                <Link
                href={"/"}><p>{"Modes de paiement"}</p></Link>
                <Link
                href={"/"}><p>{"Nous contacter"}</p></Link>
                <Link
                href={"/"}><p>{"Aide code promo Nike"}</p></Link>
                </div>
            </div>
            <div className="flex flex-col gap-2 mb-5">
            <div onClick={()=>{
                    setOpenMenu2(!openMenu2)
                }}
                    className="flex flex-row justify-between lg:pointer-events-none">
                    <p className="block">{`À PROPOS DE NIKE`}</p>
                    <PlusCircleIcon className="lg:hidden w-7"/>
                </div>
                <div  className={`font-extralight text-gray-300 text-[12px] flex flex-col gap-2 lg:block ${
                openMenu2 ? 'block' : 'hidden'
                }`}>
                <Link
                href={"/"}><p>{"Actualités"}</p></Link>
                <Link
                href={"/"}><p>{"Carrières"}</p></Link>
                <Link
                href={"/"}><p>{"Investisseurs"}</p></Link>
                <Link
                href={"/"}><p>{"Développement durable"}</p></Link>
                <Link
                href={"/"}><p>{"Mission"}</p></Link>
                </div>
            </div>
            <div className="flex flex-col items-start gap-2 mb-5">
            <div onClick={()=>{
                    setOpenMenu3(!openMenu3)
                }}
                    className="flex flex-row justify-between lg:pointer-events-none">
                    <p className="block">{`À PROPOS DE NIKE`}</p>
                    <PlusCircleIcon className="lg:hidden w-7"/>
                </div>
                <div  className={`font-extralight text-gray-300 text-[12px] flex flex-col gap-2 lg:block ${
                openMenu3 ? 'block' : 'hidden'
                }`}>
                <Link
                href={"/"}><p>{"Actualités"}</p></Link>
                <Link
                href={"/"}><p>{"Carrières"}</p></Link>
                <Link
                href={"/"}><p>{"Investisseurs"}</p></Link>
                <Link
                href={"/"}><p>{"Développement durable"}</p></Link>
                <Link
                href={"/"}><p>{"Mission"}</p></Link>
                </div>
            </div>
        </div>
        <div className=" flex flex-row space-x-3">
            <div className="w-10 h-10 bg-white rounded-full"
            ></div>
            <div className="w-10 h-10 bg-white rounded-full"
            ></div>
            <div className="w-10 h-10 bg-white rounded-full"
            ></div>
            <div className="w-10 h-10 bg-white rounded-full"
            ></div>
        </div>
    </div>
    <div className="flex flex-row justify-between text-sm">
        <div className="flex items-end">
            <p>
            <span><MapPinIcon className="w-4 inline"/>{"France"} </span>
            <span className="text-gray-300">{"c 2023 Nike,Inc. Tous droits réservés"} </span>
            </p>
        </div>    
        <div className='flex flex-col gap-4 text-[12px]'>
            <ul className="flex flex-row space-x-3  font-extralight">
                <li>{"Guides"}</li>
                <li>{`Conditions d'utilisations`}</li>
                <li>{'Conditions générales de vente'}</li>
                <li>{'Mentions légales'}</li>
            </ul>
            <ul className="flex flex-row justify-between  font-extralight">
                <li>{'Politique en matière de confidentialité et de cookies'}</li>
                <li>{'Paramètres des cookies'}</li>
            </ul>  
        </div>
    </div>
    </div>
)
}

export default Footer
