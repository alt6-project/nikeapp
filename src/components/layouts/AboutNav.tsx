import React from 'react'
import Nav from './Nav'

function AboutNav() {
return (
    <div 
        className="lg:flex hidden justify-between items-center w-full px-12 py-4 bg-black/5">
        <div
        className="flex flex-row space-x-3">
            <div
        className="w-10 h-10 bg-black rounded-full">
            </div>
            <div
            className="w-10 h-10 bg-black rounded-full">
            </div>
        </div>
        <Nav/>
    </div>
)
}

export default AboutNav
