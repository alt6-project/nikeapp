import { HeartIcon, MagnifyingGlassIcon, ShoppingBagIcon } from '@heroicons/react/24/outline'
import Link from 'next/link'
import React from 'react'
import NavDrawer from './NavDrawer'
import Search from './Search'

function Header() {
return (
    <header
    className="w-full flex justify-between items-center px-6 lg:px-12 py-4 h-20">
    <div className="">
        <div
            className="w-16 h-10 pb-4 bg-black rounded-full">
        </div>
    </div>
    <div>
        <ul className='hidden lg:flex gap-3'>
        <li className='px-2 text-s font-medium'>
            <Link
            href={'/'}
            className='hover:text-[#757575] hover:underline underline-offset-8'>
            <span>
            {'Nouveautés'}
            </span>
            </Link>
        </li>
        <li className='px-2 text-s font-medium'>
            <Link
            href={'/'}
            className='hover:text-[#757575] hover:underline underline-offset-8 '>
            <span>
            {'Homme'}
            </span>
            </Link>
        </li>
        <li className='px-2  text-s font-medium'>
            <Link
            href={'/'}
            className='hover:text-[#757575] hover:underline underline-offset-8'>
            <span>
            {'Femme'}
            </span>
            </Link>
        </li>
        <li className='px-2 text-s font-medium'>
            <Link
            href={'/'}
            className='hover:text-[#757575] hover:underline underline-offset-8'>
            <span>
            {'Enfant'}
            </span>
            </Link>
        </li>
        <li className='px-2 text-s font-medium '>
            <Link
            href={'/'}
            className='hover:text-[#757575] hover:underline underline-offset-8'>
            <span>
            {'Offres'}
            </span>
            </Link>
        </li>
        </ul>
    </div>
    <Search/>
    </header>
)
}

export default Header
