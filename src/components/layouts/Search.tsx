import { HeartIcon, MagnifyingGlassIcon, ShoppingBagIcon } from '@heroicons/react/24/outline'
import React, { useState } from 'react'
import NavDrawer from './NavDrawer'

function Search() {
    const [openDrawer, setOpenDrawer]= useState(false);
return (
    <div
    className='flex items-center w-80 gap-3 justify-end lg:justify-between'
    >
        <div
        className='hidden lg:flex flex-1 space-x-3 items-center bg-black/5 rounded-full px-3 py-2 '
        >
            <MagnifyingGlassIcon 
            className='w-7'/>

            <span
            className='opacity-20'
            >{`Rechercher`}
            </span>
        </div>
        <button
        onClick={() => setOpenDrawer(true)}
        >
            <MagnifyingGlassIcon className='icon-button lg:hidden'/>
        </button>
        <aside
        className={`fixed top-0 right-0 z-30 h-screen w-full bg-white p-6
        flex flex-col gap-3 transform ease-in-out duration-500
        ${openDrawer ? 'translate-x-0' : 'translate-x-full'}  
        `}
        >
            <div
                    className="flex w-full justify-end">
                <div
                    className=' lg:hidden flex flex-1 space-x-3 items-center bg-black/5 rounded-full px-3 py-2 '
                >
                    <MagnifyingGlassIcon className='w-7'/>
                    <span className='opacity-20'>{`Rechercher`}
                    </span>
                </div>
                <button onClick={()=>{ setOpenDrawer(false)}}>
                            Annuler
                </button>
            </div>
                <nav className='w-full'>
                    <h2 className="text-[16px] text-gray-400">Recherche poplulaires</h2>
                    <ul className='w-full flex flex-col gap-3 font-medium text-[20px] mt-2'>
                        <li>
                            <span>
                            {`Air Force1`}
                            </span>
                        </li>
                        <li>
                            <span>
                            {`Jordan`}
                            </span>
                        </li>
                        <li>
                            <span>
                            {`Air Max`}
                            </span>
                        </li>
                        <li>
                            <span>
                            {`Blazar`}
                            </span>
                        </li>
                    </ul>
                </nav>
            </aside>
        <HeartIcon 
        className='icon-button '
        />

        <ShoppingBagIcon 
        className='icon-button '
        />

        <NavDrawer/>
    </div>
)
}

export default Search
