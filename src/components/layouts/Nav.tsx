import Link from 'next/link'
import React from 'react'

function Nav() {
  return (
    <nav >
    <ul className=' flex gap-3 divide-x-2 devide-black'>
      <li className='pl-3 text-xs font-medium'>
        <Link
        href={'/'}
        className='hover:text-[#757575] hover:underline'>
          <span>
          {'Trouver un magasin'}
          </span>
        </Link>
      </li>
      <li className='pl-3 text-xs font-medium'>
        <Link
        href={'/'}
        className='hover:text-[#757575] hover:underline'>
          <span>
          {'Aide'}
          </span>
        </Link>
      </li>
      <li className='pl-3 text-xs font-medium'>
        <Link
        href={'/'}
        className='hover:text-[#757575] hover:underline'>
          <span>
          {'Nous rejoindre'}
          </span>
        </Link>
      </li>
      <li className='pl-3 text-xs font-medium'>
        <Link
        href={'/'}
        className='hover:text-[#757575] hover:underline'>
          <span>
          {"S'identifier"}
          </span>
        </Link>
      </li>
    </ul>
  </nav>
  )
}

export default Nav
