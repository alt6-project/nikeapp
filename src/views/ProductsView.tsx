import React from 'react'
import Layouts from '../components/layouts/Layouts'
import ProductMain from '../components/ProductPage/ProductMain'

function ProductsView() {
return (
    <Layouts>
        <ProductMain/>
    </Layouts>
)
}

export default ProductsView
