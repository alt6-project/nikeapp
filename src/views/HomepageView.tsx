import Link from 'next/link'
import React from 'react'
import Layouts from '../components/layouts/Layouts'
import HomePageBar from '../components/HomePage/HomePageBar'
import HomePageBanner from '../components/HomePage/HomePageBanner'
import Image from 'next/image'
import ProductCard from '../components/HomePage/ProductCard'
import ListCards from '../components/HomePage/ListCards'
import HomePageNewProductSection from '../components/HomePage/HomePageNewProductSection'


interface Props{
  products: any
}

function HomepageView({products}: Props) {
  return (
    <Layouts>
      <HomePageBar/>
      <HomePageBanner/>
      <HomePageNewProductSection products={products}/>
    </Layouts>
  )
}

export default HomepageView
