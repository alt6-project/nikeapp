import React from 'react'
import Layouts from '../components/layouts/Layouts'
import JordanTop from '../components/Jordanpage/JordanTop'

function JordanPageView() {
return (
    <Layouts>
        <JordanTop/>
    </Layouts>
)
}

export default JordanPageView
