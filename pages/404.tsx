import NotFoundView from '@/src/views/NotFoundView'
import React from 'react'

function NotFound() {
return (
    <NotFoundView/>
)
}

export default NotFound