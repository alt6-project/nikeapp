import { Inter } from 'next/font/google'
import Homepage from '@/src/views/HomepageView'
import { sanityClient } from '@/sanity' 

const inter = Inter({ subsets: ['latin'] })

interface Props{
  products: any
  variants: any
}


export default function Home({products, variants}:Props) {
  console.log('Our Variants:', variants)
  console.log(variants[0].product.slug.current);
  return (
    <Homepage 
    products={variants}/>
  )
}

// This gets called on every request
export async function getServerSideProps() {
  //Fetch Product query
  const productQuery = `
  *[_type == "product"]{
    _id,
    _createdAt,
    name,
    slug,
    mainImage,
    price,
    description,
    category->{
      name
    }
  }`;

  // Fetch data from external API
  const products = await sanityClient.fetch(productQuery)

  //Fetch Product query
  const variantQuery = `
  *[_type == "variant"]{
    product->
    {
      _id,
      _createdAt,
      name,
      slug,
      mainImage,
      price,
      description,
      category->{
        category
      }
    },
    color->{
      color
    },
    mainImage,
    images
  }`;

  // Fetch data from external API
  const variants = await sanityClient.fetch(variantQuery)
  
  // Pass data to the page via props
  return { props: 
    { 
      products,
      variants
    } }
}


