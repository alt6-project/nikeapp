import JordanPageView from '@/src/views/JordanPageView'
import React from 'react'

function index() {
    return (
        <JordanPageView/>
    )
}

export default index
