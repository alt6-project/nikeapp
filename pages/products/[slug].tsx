import React from 'react'
import { sanityClient } from '@/sanity';
import { GetStaticProps } from 'next';

interface Props{
    product:any
}
function Page({product}:Props) {
    console.log(product)
return (
    <div className="">
    <div>
        
    </div>
    <div>
        <p>Category</p>
        <p>Price</p>
    </div> 
    </div>
)
}
export default Page

export async function getStaticPaths() {
    
    const query = `*[_type == "product"]{
        _id,
        slug {
            current
        }
    } `;

    const products = await sanityClient.fetch(query);
    
    const paths = products
    .map((product: any ) =>({
        params: { slug: product?.slug?.current },
        })
    )
    .flat();
    
    return {
        paths,
        fallback: false
    }
    }
    
    export const getStaticProps: GetStaticProps = async ({params}:any) => {
        
        const productQuery= `*[_type == "product" && slug.current == $slug ][0] {
        _id,
        _createdAt,
        name,
        slug,
        mainImage,
        price,
        description,
        category->{
            name
        }
        
        }`
    
        const product = await sanityClient.fetch(productQuery, {
        slug: params?.slug,
    })
    
    if(!product){
        return {
            notFound: true
        }
    }
        
        return {
        props: {
        product,
        },
        revalidate: 10
    };
    };


